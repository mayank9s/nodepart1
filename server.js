const fs = require('fs');
const http=require('http');
const url=require('url');
const x= require('./1-node-farm/final/modules/replaceTemplate');


const templateOverview=fs.readFileSync('./1-node-farm/final/templates/template-overview.html','utf-8');
const templateCard=fs.readFileSync('./1-node-farm/final/templates/template-card.html','utf-8');
const templateProduct=fs.readFileSync('./1-node-farm/final/templates/template-product.html','utf-8');
const fileData=fs.readFileSync('./1-node-farm/final/dev-data/data.json','utf-8');
const data=JSON.parse(fileData);

const server=http.createServer((req,res)=>
{
    const {query,pathname}= url.parse(req.url,true);
        if(pathname==='/' || pathname==='/overview')
        {
            res.writeHead(202,{
                'Content-type':'text/html'
             }   )
            const cardHtml=data.map(el=> x(templateCard,el)).join('');
            const output=templateOverview.replace(/{%PRODUCT_CARDS%}/g,cardHtml);
            res.end(output);
        }
        else if(pathname==='/product')
        {
            res.writeHead(202,{
                'Content-type':'text/html'
             }   )
            const productHtml=x(templateProduct,data[query.id]);
            res.end(productHtml);
        }
        else if(pathname==='/api')
        {
            res.writeHead(202,{
               'Content-type':'Application/json'
            }   )
            res.end( fileData);
        }
        else
        {
        res.writeHead(404,{
            'Content-type':'text/html',
            'my-own-header':'Not Available'
            })
            res.end("<h1>Page not found</h1>");
        }
})

server.listen(9999,`127.0.0.1`,()=>{
    console.log("Server is listing at 9999");
})