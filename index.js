const fs=require('fs');

//Sync
const textIn=fs.readFileSync('./1-node-farm/final/txt/input.txt','utf-8');
console.log(textIn);

const textOut=`This is all about avacado ${textIn}. Created on ${Date.now()}`;
fs.writeFileSync('./1-node-farm/final/txt/output.txt',textOut);
console.log("File Written");

//Async
fs.readFile('./1-node-farm/final/txt/start.txt','utf-8',(err,data1)=>{
    console.log(data1);
    fs.readFile(`./1-node-farm/final/txt/${data1}.txt`,'utf-8',(err,data2)=>{
        console.log(data2);
        fs.readFile('./1-node-farm/final/txt/append.txt','utf-8',(err,data3)=>{
            console.log(data3);
            fs.writeFile('./1-node-farm/final/txt/final.txt',`${data2}\n ${data3}`,'utf-8',err=>{
                console.log("File Written");
            });
        });
    });
});
